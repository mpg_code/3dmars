/* Verdione - Technology for mixed-reality arts performances
 *
 * File:     featExample.cpp
 * Author:   Deepak Dwarakanath <deepakd@simula.no>
 * Contents: testapp for comparing Feature Extraction
 *
 *
 * Verdione software is released under the terms of the GNU Lesser
 * General Public License Version 2 (LGPLv2). See file COPYING.LESSER
 * for details.
 */




#include <libfacs.h>

//#include <timeutils.h>

#include <iostream>
#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>

//#include <featureextractor.h>
//#include <paramtuner.h>
//#include <performanceevaluator.h>


using namespace std;
using namespace cv;
using namespace NorthLight;
//global variable
//////CAUTION////////
///run distortion testes only with color images
///change the temp distortion format as per the dataset
//////////////////////

///opt carefully because distortion supported only for color images

int c_type = 1;//color(1)/gray(0) image capture
bool debug=true; //true for display text adn figure
bool savefig = true;
int resVar=6;//2,6,9 resize factor for blur, noise, distort



string datasetPath("../datasets/microsoft/");
string form("bmp");
int nc=8;
int ni=5;
/*

string datasetPath("../datasets/moreels/");
string form("jpg");
int nc=10;
int ni=5;


 
string datasetPath("../datasets/soccer/");
string form("jpeg");
int nc=4;
int ni=4;

 
string datasetPath("../datasets/operawide/");
string form("png");
int nc=4;
int ni=5;

string datasetPath("../datasets/operanarrow/");
string form("png");
int nc=4;
int ni=5;

string datasetPath("../datasets/poster/");
string form("png");
int nc=2;
int ni=1;
*/

int testSets = (nc-1)*ni;
const int numParams = 10;
//array initializations

Size resolutions[numParams] = {Size(160*10,120*10), Size(160*9,120*9), Size(160*8,120*8), Size(160*7,120*7), Size(160*6,120*6), Size(160*5,120*5), Size(160*4,120*4), Size(160*3,120*3), Size(160*2,120*2), Size(160*1,120*1)};
float blurlevel[numParams] = {1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0};
float noiseinDB[numParams] = {5, 10, 15, 20, 25, 30, 35, 40, 45, 50}; //in db
float distortionlevel[numParams] = {-50,-40,-30,-20,-10,10,20,30,40,50};// in percentages


/*
enum ParameterType{
	PARAM_NONE,
	PARAM_RESOLUTION,
	PARAM_BLUR,
	PARAM_NOISE,
	PARAM_DISTORTION,
};

struct measure{
    Mat measureAcc;
    Mat measureNmatch;
    Mat measureTimedetect;
    Mat measureTimedescript;
    Mat measureTimematch;
    measure(int r, int c){
        measureAcc=Mat::zeros(Size(c,r),CV_64F);
        measureNmatch =Mat::zeros(Size(c,r),CV_64F);
        measureTimedetect =Mat::zeros(Size(c,r),CV_64F);
        measureTimedescript =Mat::zeros(Size(c,r),CV_64F);
        measureTimematch =Mat::zeros(Size(c,r),CV_64F);
    }    
};
*/

void testpngwrite(vector<Mat> stereoimages){
    cout << "test image size " << stereoimages[0].size().width << ","  << stereoimages[0].size().height << endl;
    for (int val=0; val<10; val++) {
        vector<int> v;
        v.push_back(val);
        timespec ts_in, ts_out;
        ts_in = getTime();
        imwrite("outImage.png", stereoimages[0], v);
        ts_out = ts_sub(getTime(),ts_in);
        double ti = ts_out.tv_sec*MILLION + ts_out.tv_nsec/1000 ;
        cout << "time spent " << ti << endl;
        v.clear();
    }
}
/*
 struct paramRanges{	
 int res[4][2];
 paramRanges(){
 res[4][2] =  {{320*1, 240*1}, 
 {320*2, 240*2}, 
 {320*3, 240*3}, 
 {320*4, 240*4}};
 Mat resolution = Mat(4,2,CV_8U, res);
 }
 ~paramRanges(){}
 };
 */

////////////////////////////////
struct AppConfig{
    FeatureDetectorType ft;
    FeatureMatcherType mt;
    ParameterType pt;
    AppConfig(){};
    ~AppConfig(){};
};

void usage(const char* progname)
{
    cout<< "Usage: "<< progname << " [Options] images ...(1 /2 /3))\n"
    "[Options]...\n"
    "   -f, --feature  [Feature arg options] Featuretype\n"
    "   -m, --matcher  [Matcher arg options] Matchertype\n"
    "   -t, --testingtype  [Testing arg options] Matchertype\n"
    
    
    "[Feature arg options]...\n"
    "   0,    none features\n"
    "   1,    corner features\n"
    "   2,    sift features\n"
    "   3,    surf features\n"
    "   4,    star features\n"
    "   5,    orb features\n"
    
    "[Match arg options]...\n"
    "   0,    none matcher\n"
    "   1,    bruteforce matcher\n"
    "   2,    bruteforce-l1 matcher\n"
    "   3,    bruteforce-hamming matcher\n"
    "   4,    bruteforce-hamminglut matcher\n"
    "   5,    flannbased matcher\n"
    
    "[Testing arg options]...\n"
    "   0,    none\n"
    "   1,    image resolution\n"
    "   2,    image blur\n"
    "   3,    thermal noise\n"
    "   4,    geometric distortion\n"
    
    "[Help options]...\n"
    "   -h, --help   usage\n";
}


void parseCmdLine(int argc, char* argv[], AppConfig& cfg)
{
    char c;
    int long_options_index = -1;
    
    static struct option long_options[] =
    {
        {"feature", required_argument, NULL, 'f'},
        {"matcher", required_argument, NULL, 'm'},
        {"testing", required_argument, NULL, 't'},
        {"help", no_argument, NULL, 'h'},
        {0, 0, 0, 0}
    };
    
    while ((c = getopt_long (argc, argv, "f:m:t:hH", long_options, &long_options_index)) != -1)
        //   while ((c = getopt(argc, argv, "c:hH")) != -1)
    {
        switch (c)
        {
            case 'f':
                cfg.ft = (FeatureDetectorType)atoi(optarg);
                break;
            case 'm':
                cfg.mt = (FeatureMatcherType)atoi(optarg);
                break;
            case 't':
                cfg.pt = (ParameterType)atoi(optarg);
                break;
            case 'H': // wrong option
            case 'h': // wrong option
            case '?': // wrong option
                usage(argv[0]);
                exit(1);
                break;
            default:
                cout << "Error: Wrong argument " << argv[optind] << endl;
                usage(argv[0]);
                exit(1);
                break;
        };
    }
}


void displayItems(vector<cv::Mat>& img, ImageFeatures& imgst){
    
    // cout << "-------OPERATION ON " << img.size() << " image/s----------\n" << endl;
    if(imgst.isValid){cout << "--- Operation: SUCCESSFUL" << endl;}
    else{cout << "--- Operation: FAILED due to Features NOT detection" << endl;}
 
    switch(imgst.fType){
        case POINT_FEATURE_TYPE_CORNER:
            cout << "--- Feature Detector type: CORNERS" << endl;
            break;
        case POINT_FEATURE_TYPE_SIFT:
            cout << "--- Feature Detector type: SIFT" << endl;
            break;
        case POINT_FEATURE_TYPE_SURF:
            cout << "--- Feature Detector type: SURF" << endl;
            break;
        case POINT_FEATURE_TYPE_STAR:
            cout << "--- Feature Detector type: STAR" << endl;
            break;
        case POINT_FEATURE_TYPE_ORB:
            cout << "--- Feature Detector type: ORB" << endl;
            break;
        default:
            cout <<  "--- Feature Detector type: NONE" << endl;
            break;
    }
    
if(debug){
    switch(imgst.mType){
        case MATCHER_TYPE_BRUTEFORCE:
            cout << "--- Feature Matcher type: BRUTEFORCE" << endl;
            break;
        case MATCHER_TYPE_BRUTEFORCE_L1:
            cout << "--- Feature Matcher type: BRUTEFORCE_L1" << endl;
            break;
        case MATCHER_TYPE_BRUTEFORCE_HAMMING:
            cout << "--- Feature Matcher type: BRUTEFORCE_HAMMING" << endl;
            break;
        case MATCHER_TYPE_BRUTEFORCE_HAMMINGLUT:
            cout << "--- Feature Matcher type: BRUTEFORCE_HAMMINGLUT" << endl;
            break;
        case MATCHER_TYPE_FLANN:
            cout << "--- Feature Matcher type: FLANN" << endl;
            break;
        default:
            cout <<  "--- Feature Matcher type: NONE" << endl;
            break;
    }
    
    
    cout << "--- Performance on Speed" << endl;
    if(imgst.fspeed.detectionTime.tv_sec || imgst.fspeed.detectionTime.tv_nsec){
        cout << "------detection: " << (imgst.fspeed.detectionTime.tv_sec * MILLION +  imgst.fspeed.detectionTime.tv_nsec/1000) << " microsecs" << endl;
    }else{
        cout << "------detection: " << "invalid" << endl;
    }
    if(imgst.fspeed.descriptionTime.tv_sec || imgst.fspeed.descriptionTime.tv_nsec){
        cout << "------description: " << (imgst.fspeed.descriptionTime.tv_sec * MILLION +  imgst.fspeed.descriptionTime.tv_nsec/1000) << " microsecs" <<  endl;
    }else{
        cout << "------description: " << "invalid" << endl;
    }
    if(imgst.fspeed.matchingTime.tv_sec || imgst.fspeed.matchingTime.tv_nsec){
        cout << "------matching: " << (imgst.fspeed.matchingTime.tv_sec * MILLION +  imgst.fspeed.matchingTime.tv_nsec/1000) << " microsecs" <<  endl;
    }else{
        cout << "------matching: " << "invalid" << endl;
    }
    
    Mat imageMatches;
    vector<DMatch> matches;
    DMatch match;
    switch(img.size()){
        case 1:
            cout << "\n---DETECTED POINTS" << endl;
            for (uint32_t i=0;i<imgst.multikeypoints[0].size();i++){
                if(imgst.fType==POINT_FEATURE_TYPE_CORNER){cout << "ObjPoint" << i+1 << ":" << imgst.objpoints[i] << endl;}
                cout << "Point" << i+1 << ":" << imgst.multikeypoints[0][i].pt << endl;
                cout << "Index" << i+1 << ":" << imgst.matchIdxs[i][0] << endl;
            }
            break;
            
        case 2:
            cout << "--- Number of Matches:" << imgst.multikeypoints[0].size() << endl;
            if(imgst.multikeypoints[0].size()){
                //     cout << "\n--- MATCHED POINTS" << endl;
                for (uint32_t i=0;i<imgst.multikeypoints[0].size();i++){
                    match.queryIdx = i;
                    match.trainIdx = i;
                    matches.push_back(match);	
                    //              cout << "Point" << i+1 << ":" << imgst.multikeypoints[0][i].pt << endl;
                    //              cout << "Index" << i+1 << ":" << imgst.matchIdxs[i][0] << endl;
                    //              cout << "Point" << i+1 << ":" << imgst.multikeypoints[1][i].pt << endl;
                    //              cout << "Index" << i+1 << ":" << imgst.matchIdxs[i][1] << endl;
                }
            }
            
            cv::drawMatches(
                            img[0],imgst.multikeypoints[0], // 1st image and its keypoints
                            img[1],imgst.multikeypoints[1], // 2nd image and its keypoints
                            matches, // the matches
                            imageMatches, // the image produced
                            cv::Scalar(255,255,255)); // color of the lines
              
            if(savefig){
                imwrite("matches.png", imageMatches);
            }
            
            
            namedWindow("matchingImage",1);
            imshow("matchingImage", imageMatches);
            waitKey(5000);
            
            break;
            
        case 3:
            break;
    }//switch
    
}//debug
}

//
void testOnStereo(const Mat orifunmat, const vector<Mat>& stereoimages, const AppConfig& cfg, int testno, struct measure* m_result){
    
    
      
    FeatureExtractor* m_fe = new FeatureExtractor(cfg.ft, cfg.mt);
    vector<Mat> proc_images;
    vector<ImageFeatures> result_vector; 
    vector<ImageFeatures> new_result_vector; 
    
    PerformanceEvaluator eval;
    ParamTuner* tuner;
    double epiError, newepiError;
    
    Mat imageMatches;
    vector<DMatch> matches;
    DMatch match;
    
    Mat funmat;
    vector<Point2f> pts1, pts2;
    vector<Point2f> newpts1, newpts2;
    
        
    for(int i=0;i<numParams;i++){
        cout << "\n\n----TESTCASE NO: " << i+1 << " ----"  << endl;
        //clearing
        pts1.clear();
        pts2.clear();
        newpts1.clear();
        newpts2.clear();
        Mat status;
        
        //preprocess:
        /*   vector<Mat> some_images = tuner->reSize(stereoimages, resolutions[0]);
         stereoimages.clear();
         for(int i=0;i<some_images.size();i++){
         stereoimages.push_back(some_images[i]);
         }
         */  
        switch(cfg.pt){
            case PARAM_RESOLUTION:
                proc_images = tuner->reSize(stereoimages, resolutions[i]);	
                cout << "Test Resolution " << resolutions[i].width << "," << resolutions[i].height << endl;
                //         testpngwrite(proc_images);
                
                break;
            case PARAM_BLUR:
                proc_images = tuner->addBlur(tuner->reSize(stereoimages, resolutions[resVar]), blurlevel[i]);
//                proc_images = tuner->addBlur(stereoimages, blurlevel[i]);
                cout << "Test Blur level " << blurlevel[i] << endl;
                break;			   
            case PARAM_NOISE:
                proc_images = tuner->addNoise(tuner->reSize(stereoimages, resolutions[resVar]), noiseinDB[i], N_TYPE_THERMAL);
//                proc_images = tuner->addNoise(stereoimages, noiseinDB[i], N_TYPE_THERMAL);
                cout << "Test Noise level " << noiseinDB[i] << " dB" << endl;
                break;
            case PARAM_DISTORTION:
                proc_images = tuner->addDistortion(tuner->reSize(stereoimages, resolutions[resVar]), distortionlevel[i], D_TYPE_RADIAL);
//                proc_images = tuner->addDistortion(stereoimages, distortionlevel[i], D_TYPE_RADIAL);
                cout << "Test Distortion level " << distortionlevel[i] << " %" << endl;
                break;
            case PARAM_NONE:
                proc_images = stereoimages;
                cout << "Test Original " << endl;               
                break;
            default:
                break;
        };
    if (proc_images[0].data && proc_images[1].data) {
        result_vector.push_back(m_fe->N_featureDetect(proc_images));
        
        

        if(result_vector[i].multikeypoints[0].size()>7){
         //   cout << "breakpoint"<<endl;

            //performance or accuracy evaluation
            //	double error = eval->epipolarConstraintError(multikeypoints[0], multikeypoints[1]);
            cv::KeyPoint::convert( result_vector[i].multikeypoints[0], pts1);
            cv::KeyPoint::convert( result_vector[i].multikeypoints[1], pts2);
            Mat funmat = findFundamentalMat(Mat(pts1), Mat(pts2), CV_FM_RANSAC, 1.0, 0.99, status);
            vector<KeyPoint> mk1, mk2;
            vector<Vec2i> mIdxs;
            
    //        cout << status << endl;
            
            //obtain only inliers
            for(int j=0;j<status.rows;j++){
                if(status.at<bool>(j,0)){
             //       cout << "inliers" << endl;
                    mk1.push_back(result_vector[i].multikeypoints[0][j]);
                    mk2.push_back(result_vector[i].multikeypoints[1][j]);
                    mIdxs.push_back(result_vector[i].matchIdxs[j]);
                    newpts1.push_back(pts1[j]);
                    newpts2.push_back(pts2[j]);
                }
            }
            new_result_vector.push_back(result_vector[i]);
    //        cout <<"breakpoint"<<endl;
      //      cout << "debug" << new_result_vector[i].multikeypoints[0].size() << ","<< new_result_vector[i].multikeypoints[1].size()<< endl;
            
            new_result_vector[i].multikeypoints.clear();
            new_result_vector[i].matchIdxs.clear();

            new_result_vector[i].multikeypoints.push_back(mk1);
            new_result_vector[i].multikeypoints.push_back(mk2);
            new_result_vector[i].matchIdxs = mIdxs;
    //        cout << "debug" << new_result_vector[i].multikeypoints[0].size() << ","<< new_result_vector[i].multikeypoints[1].size()<< endl;
            
            //display items
                displayItems(proc_images, new_result_vector[i]);
            
            funmat = findFundamentalMat(Mat(newpts1), Mat(newpts2), CV_FM_RANSAC, 1.0, 0.99, status);
            eval.epipolarConstraintError(funmat, newpts1, newpts2, epiError);
            cout << "--- Accuracy Epipolar Error" << endl;
            cout << "------ test images Fmat: " << epiError << endl;
            eval.epipolarConstraintError(orifunmat, newpts1, newpts2, newepiError);
            cout << "------ ori images Fmat: " << newepiError << endl;
            
            //fill in the result structure
            m_result->measureAcc.at<double>(i,testno)=newepiError;
            m_result->measureNmatch.at<double>(i,testno)=new_result_vector[i].multikeypoints[0].size(); 
            m_result->measureTimedetect.at<double>(i,testno) = (new_result_vector[i].fspeed.detectionTime.tv_sec * MILLION +  new_result_vector[i].fspeed.detectionTime.tv_nsec/1000);
            m_result->measureTimedescript.at<double>(i,testno) = (new_result_vector[i].fspeed.descriptionTime.tv_sec * MILLION +  new_result_vector[i].fspeed.descriptionTime.tv_nsec/1000);
            m_result->measureTimematch.at<double>(i,testno) = (new_result_vector[i].fspeed.matchingTime.tv_sec * MILLION +  new_result_vector[i].fspeed.matchingTime.tv_nsec/1000);
            
        }else{
            cout << "------ NO / <7 features found " << endl; 
            ImageFeatures dummy;
            new_result_vector.push_back(dummy);            
        }
    }else{
        cout << "------ Image data after processing NOT found " << endl; 
        ImageFeatures dummy;
        new_result_vector.push_back(dummy);  
    }
        
       
        
    }//for(int i=0;i<numParams;i++)
    
    
  //  return m_result;

}


/////////////////////////////////////
int main(int argc, char* argv[]){
    
    cout << "---->>> testSuite - compareFeatures" << endl;
    if(argc<6){
        cout << "Error: Wrong argument " << argv[optind] << endl;
        usage(argv[0]);
        exit(1);
    }
    
    AppConfig cfg;
    parseCmdLine(argc, argv, cfg);
    
      
    vector<vector<Mat> > imageSet;
    //read the database and obtain images as vector<vector<Mat>>
    bool ret = getImagesFromDataset(datasetPath, imageSet, nc, ni, form, c_type);    
    if(!ret){
        cout << "image reading unsuccess " << endl;
        return 0;
    }else{cout << "image reading Success " << endl;}
    
    /*
    cout << "size "<<imageSet.size()<<
    imageSet[0].size()<<
    imageSet[1].size()<<
    imageSet[2].size()<<
    imageSet[3].size()<<
    endl;
    
        namedWindow("stereo 1");
     namedWindow("stereo 2");
     imshow("stereo 1", imageSet[0][1]);
     imshow("stereo 2", imageSet[4][1]);
       
    */
    
    
    
    
    struct measure* m_result = new measure(numParams, testSets);

    
    vector<Mat> stereoimages; 
    vector<Mat> p_images;    
    PerformanceEvaluator eval;
    ParamTuner* tuner;

    for (int cam=0; cam<nc-1; cam++) {
         for (int img=0; img<ni; img++) {
    //stereo images for testing
            stereoimages.clear();
             p_images.clear(); 
            stereoimages.push_back(imageSet[cam][img]);
            stereoimages.push_back(imageSet[cam+1][img]);
    
             ///take care of original first, see if >7 features can be obtained before testing
             double epiError;             Mat dummyfunmat, orifunmat, oristatus;
             vector<Point2f> pts1, pts2;
             vector<Point2f> newpts1, newpts2;
             
             //fundamental matrix for unprocessed Image
             FeatureExtractor* m_fe = new FeatureExtractor(cfg.ft, cfg.mt);
             //for first reference resolution image 1600*1200
            //saving option for orignal image
             if(savefig){
                 cout << "saving matched image" << endl;
                 p_images = tuner->reSize(stereoimages, resolutions[8]);//original images
//                 p_images = tuner->addBlur(p_images, blurlevel[8]);//+ blur images
                 p_images = tuner->addDistortion(p_images, distortionlevel[2], D_TYPE_RADIAL);//+ distort images - barrel
//                 p_images = tuner->addDistortion(p_images, distortionlevel[9], D_TYPE_RADIAL);//+ distort images - pincusion
//                   p_images = tuner->addNoise(p_images, noiseinDB[3], N_TYPE_THERMAL);//+ noise images
                 ImageFeatures imn = m_fe->N_featureDetect(p_images);
                 displayItems(p_images,imn);
             }
             ImageFeatures im = m_fe->N_featureDetect(stereoimages);
             cout << cam*ni+img << endl;
             if(im.multikeypoints[0].size()<7){
                 cout << "No / <7 features in original stereo image"<< endl;
                 continue;
             }
             
             /*
              for (int h=0;h<3;h++){
              cout << "Keypoints output" << endl;
              cout << "----------------" << endl;
              cout << "x coord: " << im.multikeypoints[0][h].pt.x << endl;
              cout << "y coord: " << im.multikeypoints[0][h].pt.y<< endl;
              cout << "size: " << im.multikeypoints[0][h].size<< endl;
              cout << "angle: " << im.multikeypoints[0][h].angle<< endl;
              cout << "response: " << im.multikeypoints[0][h].response<< endl;
              cout << "octave: " << im.multikeypoints[0][h].octave<< endl;
              cout << "class_id: " << im.multikeypoints[0][h].class_id<< endl;
              }
              */
             cv::KeyPoint::convert(im.multikeypoints[0], pts1);
             cv::KeyPoint::convert(im.multikeypoints[1], pts2);
             dummyfunmat = findFundamentalMat(Mat(pts1), Mat(pts2), CV_FM_RANSAC, 1.0, 0.99, oristatus);
             /*cout << "funmat original: " << dummyfunmat << endl;
              cout << "status original: " << status << endl;
              eval->epipolarConstraintError(dummyfunmat, im.multikeypoints[0], im.multikeypoints[1], epiError);
              cout << "Epipolar Error original" << epiError << endl;
              */	
             for(int j=0;j<oristatus.rows;j++){
                 //cout << "status2:" << status.at<bool>(j,0) << endl;
                 if(oristatus.at<bool>(j,0)){
                     newpts1.push_back(pts1[j]);
                     newpts2.push_back(pts2[j]);
                 }
             }
             
             orifunmat = findFundamentalMat(Mat(newpts1), Mat(newpts2), CV_FM_RANSAC, 1.0, 0.99, oristatus);
             //cout << "funmat: " << newfunmat << endl;
             //cout << "status: " << status << endl;
             eval.epipolarConstraintError(orifunmat, newpts1, newpts2, epiError);
             cout << "Epipolar Error original image points" << epiError << endl;
             
             
              //    if(cam*ni+img>1){
                      cout << "break" << endl;
            cout << "testing img: " << img+1 << " from cameras: " << cam+1 << " and " << cam+2 << endl;
            testOnStereo(orifunmat, stereoimages, cfg, cam*ni+img, m_result);

             cout<<"accuracy=" << m_result->measureAcc << endl;
             cout<<"nmatch=" << m_result->measureNmatch << endl;
             cout<<"dettime=" << m_result->measureTimedetect << endl;
             cout<<"desctime=" << m_result->measureTimedescript << endl;
             cout<<"matchtime=" << m_result->measureTimematch << endl;
         //    }
      }
    }
    
   
    
    /*
     cout << "waiting ...." << endl;
     waitKey(5000);
     cout << "waiting DOne" << endl;
     
     cout << "Image things" << endl;
     cout << imageSet[0][1].at<float>(0,10)<< endl;
     cout << imageSet[0][1].at<double>(0,10)<< endl;
     cout << imageSet[0][1].at<char>(0,10)<< endl;
     cout << imageSet[0][1].at<cv::Vec3b>(0,10)[0]<< endl;
     cout << imageSet[0][1].at<cv::Vec3b>(0,10)[1]<< endl;
     cout << imageSet[0][1].at<cv::Vec3b>(0,10)[2]<< endl;
     */
    
    
    waitKey(5000);
    
    
}//main


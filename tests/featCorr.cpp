
#include <iostream>
#include <sstream>
#include <string>
#include <dirent.h>
#include <errno.h>
#include <getopt.h>
#include <opencv2/opencv.hpp>

#include <stdio.h>
#include <libfacs.h>


using namespace std;
using namespace cv;
using namespace NorthLight;



struct AppCfg{
  string dirpath;
  FeatureDetectorType ft;
  FeatureMatcherType mt;
  unsigned int width;
  	unsigned int height;
  	double blur;
  	double noise;
  	double distort;
  	AppCfg(){
  		dirpath="";
  		ft=(FeatureDetectorType)0;
  		mt=(FeatureMatcherType)0;
  		width=0;
  		height=0;
  		blur=0;
  		noise=0;
  		distort=0;
  	}
};

const int numParams = 10;

Size resolutions[numParams] = {Size(160*10,120*10), Size(160*9,120*9), Size(160*8,120*8), Size(160*7,120*7), Size(160*6,120*6), Size(160*5,120*5), Size(160*4,120*4), Size(160*3,120*3), Size(160*2,120*2), Size(160*1,120*1)};
float blurlevel[numParams] = {1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0};
float noiseinDB[numParams] = {5, 10, 15, 20, 25, 30, 35, 40, 45, 50}; //in db
float distortionlevel[numParams] = {-50,-40,-30,-20,-10,10,20,30,40,50};// in percentages


 static int
 one (const struct dirent *unused){
     return 1;
 }


int scanddir(string dirpath, vector<vector<string> >& datafiles){
  struct dirent **eps, **epsi;
  int n, ni;

  n = scandir (dirpath.c_str(), &eps, one, alphasort);
//  cout << "n "<<n<< endl;
  if (n >= 0){
	  int cnt;
	  for (cnt = 2; cnt < n; ++cnt){
//	      cout << (eps[cnt]->d_name)<<endl;
		  string spath = string(dirpath)+"/"+eps[cnt]->d_name+"/";
/*		  string spath = string(dirpath);
	      spath.append("/");
	      spath.append(eps[cnt]->d_name);
	      spath.append("/");
*/
		  ni = scandir (spath.c_str(), &epsi, one, alphasort);
	      vector<string> ifiles;
	      for (int cnti = 2; cnti < ni; cnti++){
//		      cout << dirpath<<endl;
//		      cout << (eps[cnt]->d_name)<<endl;
 //           cout << (epsi[cnti]->d_name)<<endl;
				string fpath = string(dirpath)+"/"+eps[cnt]->d_name+"/"+epsi[cnti]->d_name;
/*			string fpath = string(dirpath);
		    fpath.append("/");
		    fpath.append(eps[cnt]->d_name);
		    fpath.append("/");
		    fpath.append(epsi[cnti]->d_name);
*/
	    	  ifiles.push_back(fpath);
	      }
	      datafiles.push_back(ifiles);
	  }
  }
  else{
   cout << "Couldn't open the directory" << endl;
   return -1;
  }

return 0;
}



void usage(const char* progname)
{
    cout<< "Usage: "<< progname << " [Options] images ...(1 /2 /3))\n"
    "[Options]...\n"
    "   -i, --dataset  pathToDataset\n"
	"   -f, --feature  [Feature arg options] Featuretype\n"
    "   -m, --matcher  [Matcher arg options] Matchertype\n"
//    "   -t, --testingtype  [Testing arg options] Matchertype\n"
	"   -w, value=width [160 - 1600]\n"
	"   -h, value=height [160 - 1600]\n"
	"   -b, value=blur [1.5 - 6]\n"
	"   -n, value=noise [5 - 50 dB]\n"
	"   -d, value=distortion [-50 - 50 %]\n"



	"[Feature arg options]...\n"
    "   0,    none features\n"
    "   1,    corner features\n"
    "   2,    sift features\n"
    "   3,    surf features\n"
    "   4,    star features\n"
    "   5,    orb features\n"

    "[Match arg options]...\n"
    "   0,    none matcher\n"
    "   1,    bruteforce matcher\n"
    "   2,    bruteforce-l1 matcher\n"
    "   3,    bruteforce-hamming matcher\n"
    "   4,    bruteforce-hamminglut matcher\n"
    "   5,    flannbased matcher\n"


    "[Help options]...\n"
    "   -H,   usage\n";
}


int parsecmd(int argc, char *argv[], AppCfg& cfg){

  char c;
  char* optstring = "i:f:m:w:h:b:n:d:";

  while((c=getopt(argc, argv, optstring))!=-1){
	  switch(c){
		case 'i':
			cfg.dirpath=optarg;
			break;

		case 'f':
			cfg.ft = (FeatureDetectorType)atoi(optarg);
			//cout << "cfg.ft"<<cfg.ft;
			break;

		case 'm':
			cfg.mt = (FeatureMatcherType)atoi(optarg);
			//cout << "cfg.mt"<<cfg.mt;
			break;

		case 'w':
			cfg.width = (unsigned int)atoi(optarg);
	//		cout << "cfg.width"<<cfg.width;
			break;

		case 'h':
			cfg.height = (unsigned int)atoi(optarg);
	//		cout << "cfg.height"<<cfg.height;
			break;

		case 'b':
			cfg.blur = (double)atoi(optarg);
	//		cout << "cfg.blur"<<cfg.blur;
			break;

		case 'n':
			cfg.noise = (double)atoi(optarg);
	//		cout << "cfg.noise"<<cfg.noise;
			break;

		case 'd':
			cfg.distort = (double)atoi(optarg);
	//		cout << "cfg.distort"<<cfg.distort;
			break;

		case 'H': // help
			usage(argv[0]);
			return -1;
			break;

		default:
		case '?':
			cout <<"unknown option"<<endl;
			usage(argv[0]);
			return -1;
		break;
	  };
  }

  return 0;
}

void simDist(vector<Mat>& procpair, const AppCfg &cfg){

	ParamTuner* tuner;

	if(cfg.width!=0 && cfg.height!=0){//	            case PARAM_RESOLUTION:
				procpair = tuner->reSize(procpair, Size(cfg.width, cfg.height));
		//	cout << "Test Resolution " << resolutions[i].width << "," << resolutions[i].height << endl;
			//         testpngwrite(procpair);
		}

		if(cfg.distort!=0){
		//procpair = tuner->addDistortion(tuner->reSize(procpair, resolutions[resVar]), distortionlevel[i], D_TYPE_RADIAL);
		            procpair = tuner->addDistortion(procpair, cfg.distort, D_TYPE_RADIAL);
		//cout << "Test Distortion level " << distortionlevel[i] << " %" << endl;
		}

		if(cfg.blur!=0){
		//procpair = tuner->addBlur(tuner->reSize(procpair, resolutions[resVar]), blurlevel[i]);
		            procpair = tuner->addBlur(procpair, cfg.blur);
		//cout << "Test Blur level " << blurlevel[i] << endl;
		}

		if(cfg.noise!=0){
		//procpair = tuner->addNoise(tuner->reSize(procpair, resolutions[resVar]), noiseinDB[i], N_TYPE_THERMAL);
	                procpair = tuner->addNoise(procpair, cfg.noise, N_TYPE_THERMAL);
		//	cout << "Test Noise level " << noiseinDB[i] << " dB" << endl;
		}
return;
}

int computeStereo(const vector<Mat>&imagepair, Mat& funmat, ImageFeatures& im, const AppCfg& cfg, int &outliers){

	Mat status;
	vector<Point2f> pts1, pts2;
	vector<Point2f> newpts1, newpts2;
	vector<Point2f> correctpts1, correctpts2;
	ImageFeatures temp_im;

	vector<KeyPoint> mk1, mk2;
	vector<Vec2i> mIdxs;
	vector<Vec2i> matchIdxs;

//	cout << "input cfg "<<cfg.ft<<","<<cfg.mt<<endl;
	FeatureExtractor* m_fe = new FeatureExtractor(cfg.ft, cfg.mt);
	temp_im = m_fe->N_featureDetect(imagepair);
	if(!temp_im.isValid){
		im.isValid = false;
		cout << "No valid features are detected" << endl;
		return -1;
	}else if(temp_im.multikeypoints[0].size()<7){
		im.isValid = false;
		cout << " Less than 7 features are detected" << endl;
		return -1;
	}else{

		//convert keypoints to pt coord x,y
		cv::KeyPoint::convert(temp_im.multikeypoints[0], pts1);
		cv::KeyPoint::convert(temp_im.multikeypoints[1], pts2);

		//remove outliers using ransac while estimating fundamental matrix
		funmat = findFundamentalMat(Mat(pts1), Mat(pts2), CV_FM_RANSAC, 1.0, 0.99, status);
		outliers = 0;
		 for(int j=0;j<status.rows;j++){
			 //cout << "status2:" << status.at<bool>(j,0) << endl;
			 if(status.at<bool>(j,0)){
				 newpts1.push_back(pts1[j]);
				 newpts2.push_back(pts2[j]);
				 mIdxs.push_back(temp_im.matchIdxs[j]);
                 mk1.push_back(temp_im.multikeypoints[0][j]);
                 mk2.push_back(temp_im.multikeypoints[1][j]);
			 }else
				 outliers++;
		 }

		 //Optimization based on optimal triangulation method
		 //obtain feature correspondences based on optimal 3D points.
	//	 correctMatches(funmat, newpts1, newpts2, correctpts1, correctpts2);

		 //copy temp_im into im;
		im = temp_im;
		im.matchIdxs.clear();
		im.multikeypoints.clear();
		im.multikeypoints.push_back(mk1);
		im.multikeypoints.push_back(mk2);
		im.matchIdxs = mIdxs;

	}

	return 0;
}

Mat getMatchImage(const vector<Mat>& imgs, const ImageFeatures& im){

	Mat imageMatches;
	vector<DMatch> matches;
	DMatch match;

    for (int i=0;i<im.multikeypoints[0].size();i++){
                  match.queryIdx = i;
                  match.trainIdx = i;
                  matches.push_back(match);
                  //              cout << "Point" << i+1 << ":" << imgst.multikeypoints[0][i].pt << endl;
                  //              cout << "Index" << i+1 << ":" << imgst.matchIdxs[i][0] << endl;
                  //              cout << "Point" << i+1 << ":" << imgst.multikeypoints[1][i].pt << endl;
                  //              cout << "Index" << i+1 << ":" << imgst.matchIdxs[i][1] << endl;
              }


          cv::drawMatches(
                          imgs[0],im.multikeypoints[0], // 1st image and its keypoints
                          imgs[1],im.multikeypoints[1], // 2nd image and its keypoints
                          matches, // the matches
                          imageMatches, // the image produced
                          cv::Scalar(255,255,255)); // color of the lines

          return imageMatches;
}

double meanOfVector(const vector<double> raw){
	double retval=0.0;

	for(int i=0; i<raw.size();i++){
		retval+=raw[i];
	}
	retval/=raw.size();
	return retval;
}


void prepareSaveDirs(const int n_cam, const int n_img, const string dirpath){

	string savepath;
	unsigned found = dirpath.find_last_of("/\\");
//	cout << "found"<<found<<endl;
	  //std::cout << " path: " << str.substr(0,found) << '\n';
	  //std::cout << " file: " << dirpath.substr(found+1) << endl;

//	  cout << os;
	for(int l=0; l<n_cam-1; l++){
		for(int r=l+1; r<n_cam; r++){
			//savepath.clear();
			for(int i=0; i<n_img; i++){
				ostringstream os;
				os << "mkdir -p ";
				os << dirpath.substr(found+1) << "/"<<"campair"<<"_" << l+1 <<"-"<< r+1 <<"/" <<"test"<<"_"<<i+1;
				//cout <<os.str()<< "\n"<<endl;
				system(os.str().c_str());
			}
		}
	}

	return;
}

Mat convertKeypointsToMat(const vector<KeyPoint>& keypt){
	Mat kp= Mat(keypt.size(), 6, CV_64F);
	for(int it=0;it<keypt.size();it++){
//		cout<<test_im.multikeypoints[0][it].pt.x <<","<<test_im.multikeypoints[0][it].pt.y<<
//		","<<test_im.multikeypoints[0][it].angle<<","<<test_im.multikeypoints[0][it].response<<
//		","<<test_im.multikeypoints[0][it].size<<endl;
		kp.at<double>(it,0)=keypt[it].pt.x ;
		kp.at<double>(it,1)=keypt[it].pt.y;
		kp.at<double>(it,2)=keypt[it].size;
		kp.at<double>(it,3)=keypt[it].angle;
		kp.at<double>(it,4)=keypt[it].response;
	}

	return kp;
}

void storeData(const vector<Mat> &procpair,
			 const ImageFeatures& test_im,
			 const vector<Point2f>& test_pts1,
			 const vector<Point2f>&test_pts2,
			 const Mat& test_funmat,
			 const double &epiError,
			 const double &test_outliers,
			 const string &save){

	//save stereo pair image matches
	Mat imageMatches = getMatchImage(procpair, test_im);
	string matchfile = save + "_match.png";
	imwrite(matchfile.c_str(), imageMatches);

	//save measurements
	Mat measure = Mat(6,1,CV_64F);
	measure.at<double>(0,0)=epiError;
	measure.at<double>(1,0)=test_pts1.size();
	measure.at<double>(2,0)=(test_im.fspeed.detectionTime.tv_sec * MILLION +  test_im.fspeed.detectionTime.tv_nsec/1000);
	measure.at<double>(3,0)=(test_im.fspeed.descriptionTime.tv_sec * MILLION +  test_im.fspeed.descriptionTime.tv_nsec/1000);
	measure.at<double>(4,0)=(test_im.fspeed.matchingTime.tv_sec * MILLION +  test_im.fspeed.matchingTime.tv_nsec/1000);
	measure.at<double>(5,0)=test_outliers;

	string measurefile = save + "_measure.xml";
	FileStorage mf(measurefile.c_str(), FileStorage::WRITE);
	mf << "accuracy-nMatch-dettime-desctime-matchtime-outliers" << measure;
	mf.release();

	//save fundamental matrix
	string funmatfile = save + "_funmat.xml";
	FileStorage ff(funmatfile.c_str(), FileStorage::WRITE);
		ff << "funmat" << test_funmat;
	ff.release();

	//save keypoints
	Mat kp1 = convertKeypointsToMat(test_im.multikeypoints[0]);
	Mat kp2 = convertKeypointsToMat(test_im.multikeypoints[1]);
//	cout << kp1 << endl;
//	cout << kp2 << endl;
	string keyfile = save + "_keypoint.xml";
	FileStorage ims(keyfile.c_str(), FileStorage::WRITE);
		ims <<"keyPoints1"<<kp1;
		ims <<"keyPoints2"<<kp2;
	ims.release();


	return;
}

int main(int argc, char *argv[]){

	cout <<"Testing how good the features are?"<<endl;

	/*passing commandline arguments*/
	if(argc<7){
		usage(argv[0]);
		return -1;
	}
	struct AppCfg cfg;
	if(parsecmd(argc, argv, cfg)!=0)
		return -1;

	/*Feedback for user*/
	cout << "datapath: "<< cfg.dirpath << endl;
	cout << "feature: "<< cfg.ft << endl;
	cout << "matching: "<< cfg.mt << endl;
	cout << "resolution: "<< cfg.width << ","<< cfg.height << endl;
	cout << "distortion: "<< cfg.distort << endl;
	cout << "noise: "<< cfg.noise << endl;
	cout << "blur: "<< cfg.blur << endl;


	/*Read the directory structure and obtain absolute paths*/
	//cout << "dir is :"<< cfg.dirpath <<endl;
	vector <vector<string> > datafiles;
	scanddir(cfg.dirpath, datafiles);//read list of directories related to cameras
	int n_cam = datafiles.size();
	int n_img = datafiles[0].size();
	cout << "Reading the dataset consisting of :\n"<<
		"cameras: "<<n_cam<<"\n"<<
		"images: "<<n_img<<endl;

	prepareSaveDirs(n_cam, n_img, cfg.dirpath);
	ostringstream savestring;
	savestring << "f"<<cfg.ft<<
				  "-m"<<cfg.mt<<
				  "-res"<<cfg.width<<"x"<<cfg.height<<
				  "-d"<<cfg.distort<<
				  "-n"<<cfg.noise<<
				  "-b"<<cfg.blur;

//cout << "savestring: "<<savestring.str()<<endl;

	/*for(int i=0;i<n_cam;i++){
	  for(vector<string>::iterator it = datafiles[i].begin(); it != datafiles[i].end();++it)
		cout << *it << endl;
	   }
	*/

	//prepare storage of results
	struct measure* m_result = new measure(n_cam-1,n_cam);

	/*Read images in stereo from a pair*/
	//img 0 from cam0, cam1
for(int l=0; l<n_cam-1; l++){
	for(int r=l+1; r<n_cam; r++){
		 vector<double> accuracy;
		 vector<double> nmatch;
		 vector<double> dettime;
		 vector<double> desctime;
		 vector<double> matchtime;
		 vector<double> outliers;

		for(int i=0; i<n_img; i++){
			cout << "Processing ....\n"
				"img: "<<i+1<<" of cam :"<<l+1<<","<<r+1<<endl;

			Mat left = imread(datafiles[l][i]);
			Mat right = imread(datafiles[r][i]);

			vector<Mat> stereopair, procpair;
			stereopair.push_back(left);
			stereopair.push_back(right);
			if(!stereopair[0].data || !stereopair[1].data){
				cout << "Sorry no image is read"<<endl;
				return -1;
			}

			procpair = stereopair;

			//cout << procpair[0].cols <<","<< procpair[0].rows<<endl;
			//cout << procpair[1].cols <<","<< procpair[1].rows<<endl;
			/*
			cout << stereopair[0].cols <<","<< stereopair[0].rows<<endl;
			cout << stereopair[1].cols <<","<< stereopair[1].rows<<endl;
			namedWindow("leftimage");
			namedWindow("rightimage");
			for(;;){
				imshow("leftimage", stereopair[0]);
				imshow("rightimage", stereopair[1]);
				waitKey(10);
			}*/


			//Simulate camera internal disturbances
			simDist(procpair, cfg);
		/*
			namedWindow("proc0",1);
			namedWindow("proc1",1);
			for(;;){
				imshow("proc0",procpair[0]);
				imshow("proc1",procpair[1]);
				waitKey(0);
			}
		*/


			/*Feature extraction*/
			Mat ori_funmat;
			ImageFeatures ori_im;
			int ori_outliers;
			int rets=computeStereo(stereopair, ori_funmat, ori_im, cfg, ori_outliers);

			//cout<< "ori_im.isValid"<<ori_im.isValid<<endl;

			Mat test_funmat;
			ImageFeatures test_im;
			int test_outliers;
			int retp=computeStereo(procpair, test_funmat, test_im, cfg, test_outliers);

			//cout<< "test_im.isValid"<<test_im.isValid<<endl;


			if(ori_im.isValid && test_im.isValid){// || rets!=0 || retp!=0){

				/*Evaluate performance*/
				PerformanceEvaluator eval;
				double epiError;
				vector<Point2f> test_pts1, test_pts2;
				//convert keypoints to pt coord x,y
				cv::KeyPoint::convert(test_im.multikeypoints[0], test_pts1);
				cv::KeyPoint::convert(test_im.multikeypoints[1], test_pts2);


				eval.epipolarConstraintError(ori_funmat, test_pts1, test_pts2, epiError);

				 //ransac time is not mentioned below
		/*		 cout<<"accuracy=" << epiError << endl;
				 cout<<"nmatch=" << test_pts1.size() << endl;
				 cout<<"dettime=" << (test_im.fspeed.detectionTime.tv_sec * MILLION +  test_im.fspeed.detectionTime.tv_nsec/1000) << endl;
				 cout<<"desctime=" << (test_im.fspeed.descriptionTime.tv_sec * MILLION +  test_im.fspeed.descriptionTime.tv_nsec/1000) << endl;
				 cout<<"matchtime=" << (test_im.fspeed.matchingTime.tv_sec * MILLION +  test_im.fspeed.matchingTime.tv_nsec/1000) << endl;
				 cout<<"outliers="<<test_outliers<<endl;
		 */

				/*measurement*/
				 accuracy.push_back(epiError);
				 nmatch.push_back(test_pts1.size());
				 dettime.push_back((test_im.fspeed.detectionTime.tv_sec * MILLION +  test_im.fspeed.detectionTime.tv_nsec/1000));
				 desctime.push_back((test_im.fspeed.descriptionTime.tv_sec * MILLION +  test_im.fspeed.descriptionTime.tv_nsec/1000));
				 matchtime.push_back((test_im.fspeed.matchingTime.tv_sec * MILLION +  test_im.fspeed.matchingTime.tv_nsec/1000));
				 outliers.push_back(test_outliers);
				/* Visualize matches*/
			//	 Mat imageMatches = getMatchImage(procpair, test_im);
			//	 imwrite("matches.png", imageMatches);
			/*    namedWindow("matchingImage",1);
				imshow("matchingImage", imageMatches);
				waitKey(5000);
			*/

				 //Storing
				 unsigned found = cfg.dirpath.find_last_of("/\\");
				 ostringstream savepath;
				 savepath << cfg.dirpath.substr(found+1) << "/"<<"campair"<<"_" << l+1 <<"-"<< r+1 <<"/" <<"test"<<"_"<<i+1<<"/";
			//	 cout << savepath.str() << savestring.str()<<endl;
				 string save = savepath.str()+savestring.str();
				 storeData(procpair, test_im, test_pts1, test_pts2, test_funmat, epiError, test_outliers, save);


			}else{
				cout << "Could not detect features for image: " << i+1 << endl;
			}

		}//loop over images

		//fill in the result structure
		m_result->measureAcc.at<double>(l,r) = meanOfVector(accuracy);
		m_result->measureNmatch.at<double>(l,r) = floor(meanOfVector(nmatch));
		m_result->measureTimedetect.at<double>(l,r) = meanOfVector(dettime);
		m_result->measureTimedescript.at<double>(l,r) = meanOfVector(desctime);
		m_result->measureTimematch.at<double>(l,r) = meanOfVector(matchtime);
		m_result->measureOutliers.at<double>(l,r) = meanOfVector(outliers);

	}
}

cout<<"accuracy =" << m_result->measureAcc << endl;
cout<<"nmatch =" << m_result->measureNmatch << endl;
cout<<"dettime =" << m_result->measureTimedetect << endl;
cout<<"desctime =" << m_result->measureTimedescript << endl;
cout<<"matchtime =" << m_result->measureTimematch << endl;
cout<<"outliers =" << m_result->measureOutliers << endl;

/*
FileStorage fs("nmatchyml.yml", FileStorage::WRITE);
fs << "nMatch" << m_result->measureNmatch;
fs.release();
*/
unsigned found = cfg.dirpath.find_last_of("/\\");
string overallfile = cfg.dirpath.substr(found+1) + "/" + savestring.str() + "_overallMeasure.xml";

FileStorage fas(overallfile.c_str(), FileStorage::WRITE);
	fas << "accuracy" << m_result->measureAcc;
	fas << "nMatch" << m_result->measureNmatch;
	fas <<"dettime" << m_result->measureTimedetect;
	fas <<"desctime" << m_result->measureTimedescript;
	fas <<"matchtime" << m_result->measureTimematch;
	fas <<"outliers" << m_result->measureOutliers;
fas.release();









///////////////////////////////
	 return 0;
}

	/*
			if(cfg.pt==PARAM_NONE){
				 ori_funmat = findFundamentalMat(Mat(newpts1), Mat(newpts2), CV_FM_RANSAC, 1.0, 0.99, status);
				 FileStorage fsw("ori_funmat.yml", FileStorage::WRITE);
				 fsw << "ori_funmat" << ori_funmat;
				 fsw.release();
				 eval.epipolarConstraintError(ori_funmat, newpts1, newpts2, epiError);

			 }else {//if not, then using ori_funmat estimate accuracy of simulated stereo pair
				 FileStorage fsr("ori_funmat.yml", FileStorage::READ);
				 Mat ofunmat;
				 fsr["ori_funmat"] >> ofunmat;
				 cout << ofunmat << endl;
				 fsr.release();
				 eval.epipolarConstraintError(ofunmat, newpts1, newpts2, epiError);


			 }
	*/




		 /*
struct measure* m_result = new measure(numParams, testSets);
		 //fill in the result structure
		            m_result->measureAcc.at<double>(i,testno)=newepiError;
		            m_result->measureNmatch.at<double>(i,testno)=new_result_vector[i].multikeypoints[0].size();
		            m_result->measureTimedetect.at<double>(i,testno) = (new_result_vector[i].fspeed.detectionTime.tv_sec * MILLION +  new_result_vector[i].fspeed.detectionTime.tv_nsec/1000);
		            m_result->measureTimedescript.at<double>(i,testno) = (new_result_vector[i].fspeed.descriptionTime.tv_sec * MILLION +  new_result_vector[i].fspeed.descriptionTime.tv_nsec/1000);
		            m_result->measureTimematch.at<double>(i,testno) = (new_result_vector[i].fspeed.matchingTime.tv_sec * MILLION +  new_result_vector[i].fspeed.matchingTime.tv_nsec/1000);

*/


	/*	 for(int j=0;j<pts1.size();j++){
				cout << "x" << pts1[j].x << "y"<< pts1[j].y;
			}
*/
		/* FileStorage fs("test.yml", FileStorage::WRITE);
		//fs << "funmat" << funmat;


		fs << "keypoints" << "[";

		for(int j=0;j<pts1.size();j++){
			fs << "{:" << "x" << pts1[j].x << "y"<< pts1[j].y;

		fs << "}";
		}
		fs << "]";
		fs.release();

	}*///else
      /*
       for (int h=0;h<3;h++){
       cout << "Keypoints output" << endl;
       cout << "----------------" << endl;
       cout << "x coord: " << im.multikeypoints[0][h].pt.x << endl;
       cout << "y coord: " << im.multikeypoints[0][h].pt.y<< endl;
       cout << "size: " << im.multikeypoints[0][h].size<< endl;
       cout << "angle: " << im.multikeypoints[0][h].angle<< endl;
       cout << "response: " << im.multikeypoints[0][h].response<< endl;
       cout << "octave: " << im.multikeypoints[0][h].octave<< endl;
       cout << "class_id: " << im.multikeypoints[0][h].class_id<< endl;
       }
       */
